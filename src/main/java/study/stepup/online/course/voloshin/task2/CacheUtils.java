package study.stepup.online.course.voloshin.task2;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class CacheUtils {

    public static <T> T cache(T obj) {
        return (T) Proxy.newProxyInstance(
                obj.getClass().getClassLoader(),
                obj.getClass().getInterfaces(),
                new CacheHandler(obj));
    }

    static class CacheHandler implements InvocationHandler {

        HashMap<ArrayList<Object>, Object> cachedValues = new HashMap<>();
        Object obj;

        public CacheHandler(Object obj) {
            this.obj = obj;
        }


        @Override
        public Object invoke(Object proxy, Method interfaceMethod, Object[] args) throws Throwable {
            Method classMethod = obj.getClass().getMethod(interfaceMethod.getName(),
                    interfaceMethod.getParameterTypes());
            if (classMethod.isAnnotationPresent(Mutator.class)) {
                cachedValues.clear();
                return classMethod.invoke(obj, args);
            } else if (classMethod.isAnnotationPresent(Cache.class)) {
                ArrayList<Object> key = new ArrayList<>();
                key.add(classMethod);
                if (args != null) {
                    Collections.addAll(key, args);
                }
                if (cachedValues.containsKey(key)) {
                    return cachedValues.get(key);
                } else {
                    Object value = classMethod.invoke(obj, args);
                    cachedValues.put(key, value);
                    return value;
                }
            } else {
                return classMethod.invoke(obj, args);
            }
        }
    }
}
