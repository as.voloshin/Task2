package study.stepup.online.course.voloshin.task2;


public interface TestClassable {
    int getFieldInt1();

    void setFieldInt1(int fieldInt1);

    int getFieldInt2();

    void setFieldInt2(int fieldInt2);

    String getFieldString();

    void setFieldString(String fieldString);

    String toString();

    Integer testMethod1();

    Integer testMethod1(Integer x, Integer y);
}
