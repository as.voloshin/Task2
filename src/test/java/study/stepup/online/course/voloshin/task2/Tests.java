package study.stepup.online.course.voloshin.task2;

import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Tests {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    /**
     * Все методы класса TestClass отправляют сообщения с помощью System.out.print
     * Мы ловим эти сообщения в outContent и проверяем содержимое стрима,
     * чтобы выяснить, вызывался ли оригинальный метод или использовался кэш.
     */

    @BeforeEach
    public void setUpStream() {
        System.setOut(new PrintStream(outContent));
        outContent.reset();
    }

    @AfterEach
    public void restoreStream() {
        System.setOut(originalOut);
    }

    public boolean checkOutputEqualsString(String str) {
        String out = outContent.toString();
        outContent.reset();
        return str.equals(out);
    }

    @Test
    @DisplayName("Проверяем, что первый вызов метода без аннотаций вызывает оригинальный метод")
    public void checkFirstCallOfNotAnnotatedMethod() {
        TestClassable tst = new TestClass();
        tst = CacheUtils.cache(tst);

        tst.getFieldInt2();
        Assertions.assertTrue(checkOutputEqualsString("getFieldInt2 method was called."));

        tst.setFieldInt2(5);
        Assertions.assertTrue(checkOutputEqualsString("setFieldInt2 method was called."));
    }

    @Test
    @DisplayName("Проверяем, что повторный вызов метода без аннотаций вызывает оригинальный метод")
    public void checkNextCallOfNotAnnotatedMethod() {
        TestClassable tst = new TestClass();
        tst = CacheUtils.cache(tst);

        tst.getFieldInt2();
        outContent.reset();
        tst.getFieldInt2();
        Assertions.assertTrue(checkOutputEqualsString("getFieldInt2 method was called."));
        tst.getFieldInt2();
        Assertions.assertTrue(checkOutputEqualsString("getFieldInt2 method was called."));
        tst.getFieldInt2();
        Assertions.assertTrue(checkOutputEqualsString("getFieldInt2 method was called."));

        tst.setFieldInt2(5);
        outContent.reset();
        tst.setFieldInt2(6);
        Assertions.assertTrue(checkOutputEqualsString("setFieldInt2 method was called."));
        tst.setFieldInt2(7);
        Assertions.assertTrue(checkOutputEqualsString("setFieldInt2 method was called."));
    }

    @Test
    @DisplayName("Проверяем, что первый вызов метода с аннотацией @Cache вызывает оригинальный метод")
    public void checkFirstCallOfCacheAnnotatedMethod() {
        TestClassable tst = new TestClass();
        tst = CacheUtils.cache(tst);

        tst.getFieldInt1();
        Assertions.assertTrue(checkOutputEqualsString("getFieldInt1 method was called."));

        tst.getFieldString();
        Assertions.assertTrue(checkOutputEqualsString("getFieldString method was called."));

        tst.toString();
        Assertions.assertTrue(checkOutputEqualsString("toString method was called."));

        tst.testMethod1();
        Assertions.assertTrue(checkOutputEqualsString("testMethod1 (no arguments) method was called."));

        tst.testMethod1(20, 30);
        Assertions.assertTrue(checkOutputEqualsString("testMethod1 (2 arguments) method was called."));
    }

    @Test
    @DisplayName("Проверяем, что повторный вызов метода с аннотацией @Cache не вызывает оригинальный метод")
    public void checkNextCallOfCacheAnnotatedMethod() {
        TestClassable tst = new TestClass();
        tst = CacheUtils.cache(tst);

        tst.getFieldInt1();
        outContent.reset();
        tst.getFieldInt1();
        Assertions.assertFalse(checkOutputEqualsString("getFieldInt1 method was called."));
        tst.getFieldInt1();
        Assertions.assertFalse(checkOutputEqualsString("getFieldInt1 method was called."));
        tst.getFieldInt1();
        Assertions.assertFalse(checkOutputEqualsString("getFieldInt1 method was called."));

        tst.getFieldString();
        outContent.reset();
        tst.getFieldString();
        Assertions.assertFalse(checkOutputEqualsString("getFieldString method was called."));

        tst.toString();
        outContent.reset();
        tst.toString();
        Assertions.assertFalse(checkOutputEqualsString("toString method was called."));

        tst.testMethod1();
        outContent.reset();
        tst.testMethod1();
        Assertions.assertFalse(checkOutputEqualsString("testMethod1 (no arguments) method was called."));

        tst.testMethod1(20, 30);
        outContent.reset();
        tst.testMethod1(20, 30);
        Assertions.assertFalse(checkOutputEqualsString("testMethod1 (2 arguments) method was called."));
    }

    @Test
    @DisplayName("Проверяем, что первый вызов метода с аннотацией @Mutator вызывает оригинальный метод")
    public void checkFirstCallOfMutatorAnnotatedMethod() {
        TestClassable tst = new TestClass();
        tst = CacheUtils.cache(tst);

        tst.setFieldInt1(5);
        Assertions.assertTrue(checkOutputEqualsString("setFieldInt1 method was called."));

        tst.setFieldString("New Value");
        Assertions.assertTrue(checkOutputEqualsString("setFieldString method was called."));
    }

    @Test
    @DisplayName("Проверяем, что повторный вызов метода с аннотацией @Mutator вызывает оригинальный метод")
    public void checkNextCallOfMutatorAnnotatedMethod() {
        TestClassable tst = new TestClass();
        tst = CacheUtils.cache(tst);

        tst.setFieldInt1(5);
        outContent.reset();
        tst.setFieldInt1(-3);
        Assertions.assertTrue(checkOutputEqualsString("setFieldInt1 method was called."));
        tst.setFieldInt1(-4);
        Assertions.assertTrue(checkOutputEqualsString("setFieldInt1 method was called."));
        tst.setFieldInt1(-5);
        Assertions.assertTrue(checkOutputEqualsString("setFieldInt1 method was called."));

        tst.setFieldString("New Value");
        outContent.reset();
        tst.setFieldString("Another New Value");
        Assertions.assertTrue(checkOutputEqualsString("setFieldString method was called."));
    }

    @Test
    @DisplayName("Проверяем, что повторный вызов метода с аннотацией @Cache возвращает корректное значение")
    public void checkCachedValueICorrect() {
        TestClassable tst = new TestClass();
        tst = CacheUtils.cache(tst);
        int x;
        String s;

        tst.getFieldInt1();
        x = tst.getFieldInt1();
        Assertions.assertEquals(x, 0);

        tst.getFieldString();
        s = tst.getFieldString();
        Assertions.assertEquals(s, "Test");

        tst.toString();
        s = tst.toString();
        Assertions.assertEquals(s, "TestClass{fieldInt1=0, fieldInt2=1, fieldString='Test'}");

        tst.testMethod1();
        x = tst.testMethod1();
        Assertions.assertEquals(x, 1);

        tst.setFieldInt2(5);
        s = tst.toString();
        Assertions.assertEquals(s, "TestClass{fieldInt1=0, fieldInt2=1, fieldString='Test'}");
        x = tst.testMethod1();
        Assertions.assertEquals(x, 1);

        tst.testMethod1(20, 30);
        x = tst.testMethod1(20, 30);
        Assertions.assertEquals(x, 25);
    }

    @Test
    @DisplayName("Проверяем, что повторный вызов метода с аннотацией @Mutator очищает кэш")
    public void checkCallOfMutatorAnnotatedMethodClearsCache() {
        TestClassable tst = new TestClass();
        tst = CacheUtils.cache(tst);

        tst.getFieldInt1();
        tst.getFieldString();
        tst.toString();
        tst.setFieldInt1(5);
        outContent.reset();

        tst.getFieldInt1();
        Assertions.assertTrue(checkOutputEqualsString("getFieldInt1 method was called."));

        tst.getFieldString();
        Assertions.assertTrue(checkOutputEqualsString("getFieldString method was called."));

        tst.toString();
        Assertions.assertTrue(checkOutputEqualsString("toString method was called."));

        tst.testMethod1();
        Assertions.assertTrue(checkOutputEqualsString("testMethod1 (no arguments) method was called."));

        tst.testMethod1(20, 30);
        Assertions.assertTrue(checkOutputEqualsString("testMethod1 (2 arguments) method was called."));
    }

}
