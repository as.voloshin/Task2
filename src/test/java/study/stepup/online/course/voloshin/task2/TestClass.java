package study.stepup.online.course.voloshin.task2;


public class TestClass implements TestClassable {
    private int fieldInt1, fieldInt2;
    private String fieldString;

    public TestClass() {
        this.fieldInt1 = 0;
        this.fieldInt2 = 1;
        this.fieldString = "Test";
    }

    @Cache
    public int getFieldInt1() {
        System.out.print("getFieldInt1 method was called.");
        return fieldInt1;
    }

    @Mutator
    public void setFieldInt1(int fieldInt1) {
        System.out.print("setFieldInt1 method was called.");
        this.fieldInt1 = fieldInt1;
    }

    public int getFieldInt2() {
        System.out.print("getFieldInt2 method was called.");
        return fieldInt2;
    }

    public void setFieldInt2(int fieldInt2) {
        System.out.print("setFieldInt2 method was called.");
        this.fieldInt2 = fieldInt2;
    }

    @Cache
    public String getFieldString() {
        System.out.print("getFieldString method was called.");
        return fieldString;
    }

    @Mutator
    public void setFieldString(String fieldString) {
        System.out.print("setFieldString method was called.");
        this.fieldString = fieldString;
    }

    @Override
    @Cache
    public String toString() {
        System.out.print("toString method was called.");
        return "TestClass{" +
                "fieldInt1=" + fieldInt1 +
                ", fieldInt2=" + fieldInt2 +
                ", fieldString='" + fieldString + '\'' +
                '}';
    }

    @Cache
    public Integer testMethod1() {
        System.out.print("testMethod1 (no arguments) method was called.");
        return this.fieldInt1 + this.fieldInt2;
    }

    @Cache
    public Integer testMethod1(Integer x, Integer y) {
        System.out.print("testMethod1 (2 arguments) method was called.");
        return this.fieldInt1 + this.fieldInt2 + x;
    }

}
